package cartabaralla;

public class ProvaBaralla {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Baralla baralla = new Baralla();
		Carta c;
		int i;
		
		System.out.println(baralla);
		for (i=0; i<5; i++) {
			baralla.remena();
			System.out.println(baralla);
		}
		System.out.println("Traiem les primeres 10 cartes: ");
		for (i=0; i<10; i++) {
			c = baralla.roba();
			System.out.println("Carta: "+c+" Baralla: "+baralla);
		}
		
		System.out.println("Eliminem les figures: ");
		baralla.eliminaNum(10);
		baralla.eliminaNum(11);
		baralla.eliminaNum(12);
		System.out.println(baralla);
	}

}
