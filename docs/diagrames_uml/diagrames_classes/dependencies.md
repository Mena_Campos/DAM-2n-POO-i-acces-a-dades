## Dependències

Existeix una **dependència** entre dos elements si els canvis en la
definició d'un dels elements poden causar canvis en l'altre. Entre
classes, les dependències poden existir bé perquè un classe envia un
missatge a una altra, bé perquè una classe en té una altra com a part de
les seves dades, o bé perquè una classe en menciona una altra com a
paràmetre d'una operació. Si una classe canvia la seva interfície,
qualsevol missatge que s'envia a aquesta classe pot deixar de ser vàlid.

![Exemple dependència](docs/diagrames_uml/imatges/exemple_dependencia.png)

Els diagrames UML permeten mostrar dependències entre qualsevol parell
d'elements.

En un programa extens, no és viable mostrar totes les dependències
existents. Aleshores cal triar aquelles dependències que són rellevants a
allò que es vol mostrar.

Per veure les dependències a un nivell més alt, s'utilitza un *diagrama
de paquets*.

### Agregació i composició

L'agregació i la composició són dos tipus de dependències molt habituals
que utilitzen els seus propis símbols en els diagrames.

Les dues relacions s'utilitzen per indicar una inclusió dels objectes
d'una classe en un objecte d'una altra classe.

L'**agregació** representa una relació de tipus *és-part-de*. Per exemple,
els socis formen part d'un club, o uns clients formen part de la cartera
de clients d'una empresa. L'agregació no és un concepte ben definit
i es confon fàcilment amb una *associació*, per aquest motiu hi ha qui
recomana no utilitzar aquesst tipus de relació en els diagrames.

![Exemple agregació](docs/diagrames_uml/imatges/exemple_agregacio.png)

La **composició** és un tipus de relació que implica exclusivitat. Per
exemple, un punt pot ser part d'un polígon o pot ser el centre d'un
cercle, però no les dues coses a la vegada; o una agenda inclou les seves
pàgines.

![Exemple composició](docs/diagrames_uml/imatges/exemple_composicio.png)

Per tal que una relació es consideri una composició cal que es compleixin
algunes condicions:

* Un dels objectes forma part de l'altre, de manera que si s'esborra
el segon, no té sentit que el primer segueixi existing.

* La multiplicitat de la classe inclosa respecte a la classe incloent
sempre és 0..1 o 1. És a dir, un objecte de la classe inclosa només pot
pertànyer a un objecte de la classe incloent.