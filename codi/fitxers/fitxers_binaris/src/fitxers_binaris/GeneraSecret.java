package fitxers_binaris;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class GeneraSecret {

	private static final int N_CODIS = 1000;
	
	public static void generaFitxer(String nomFitxer) {
		// generem conjunt de lletres minúscules
		StringBuilder builder = new StringBuilder();
		for (char c='a'; c<'z'; c++)
			builder.append(c);
		String lletres = builder.toString();
		
		// creem el fitxer
		Random random = new Random();
		int codi=0;
		String secret;
		
		try (DataOutputStream escriptor = new DataOutputStream(new FileOutputStream(nomFitxer))) {
			for (int i=0; i<N_CODIS; i++) {
				// generem valors aleatoris
				codi+=random.nextInt(3)+1;
				secret="";
				for (int j=0; j<3; j++)
					secret += lletres.charAt(random.nextInt(lletres.length()));
				// guardem al fitxer
				escriptor.writeInt(codi);
				escriptor.writeChars(secret);
				//System.out.println(codi+" "+secret);
			}
		} catch (FileNotFoundException e) {
			System.err.println("No es pot obrir el fitxer: "+e.getMessage());
		} catch (IOException e) {
			System.err.println("Error al crear el fitxer: "+e.getMessage());
		}
	}

	public static void main(String[] args) {
		generaFitxer("secret.bin");
	}

}
