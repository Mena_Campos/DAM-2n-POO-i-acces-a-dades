import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

//Fes un programa que permeti fer cerques de pel�l�cules per paraula clau. 
//El programa ha de cercar la paraula clau en el t�tol i en la descripci�.
//Per cada pel�l�cula trobada, s'ha d'indicar a quines botigues es pot trobar.
public class Exercici_2_4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
        String word=" ";
        String url = "jdbc:mysql://localhost:3306/sakila";
        Properties connectionProperties = new Properties();
        connectionProperties.setProperty("user", "root");
        try (Connection con = DriverManager.getConnection(url,connectionProperties);
        		PreparedStatement st = con.prepareStatement("select f.title, f.description, i.store_id from film f"
        				+ " left join inventory i on (f.film_id = i.film_id) "
        				+ "where ((upper(title) like ?) || (upper(description) like ?))"
        				+ "order by 3")) {
            System.out.println("Base de dades connectada!");
            while (!word.equals("")) {
                System.out.println("Paraula en clau a cercar en MAYUS (en blanc per sortir): ");
                word = sc.nextLine();
                if (!word.equals("")) {
                    st.setString(1, "%" + word + "%");
                    st.setString(2, "%" + word + "%");
                    try (ResultSet rs = st.executeQuery()) {
                        while (rs.next()) {
                            String title = rs.getString("title");
                            String description = rs.getString("description");
                            int store_id = rs.getInt("store_id");
                            System.out.println(store_id + "\t" + title + "\t"+ description);
                        }
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println("Error SQL: " + e.getMessage());
        }

        sc.close();
	}

}
