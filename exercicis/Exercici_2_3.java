import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.Scanner;

//Fes un programa que permeti cercar un client i ens mostri tots els pagaments que ha fet entre dues dates donades. 
//De cada pagament es mostrar� la quantitat, la data, 
//i el nom de l'�tem que s'havia llogat (si �s el cas, hi ha pagaments que no estan associats a cap �tem).
public class Exercici_2_3 {

	public static void main(String[] args) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		Scanner sc = new Scanner(System.in);
		String firstName=" ";
		String lastName=" ";
		String dateMin=" ";
		String dateMax=" ";
        String url = "jdbc:mysql://localhost:3306/sakila";
        Properties connectionProperties = new Properties();
        connectionProperties.setProperty("user", "root");
        try (Connection con = DriverManager.getConnection(url,connectionProperties);
        		PreparedStatement st = con.prepareStatement(
                        "select f.title, p.amount, p.payment_date from payment p "
                        + "left join customer c on(p.customer_id = c.customer_id) "
                        + "left join rental r on(p.rental_id = r.rental_id) "
                        + "left join inventory i on(r.inventory_id = i.inventory_id) "
                        + "left join film f on(i.film_id = f.film_id) "
                        + "where ((upper(c.first_name) like '?') && (upper(c.last_name) like '?') && (DATE(p.payment_date) BETWEEN '?' AND '?') && (p.payment_date IS NOT NULL))")) {
            System.out.println("Base de dades connectada!");
            while (!lastName.equals("")) {
                System.out.println("Nom a cercar (en blanc per sortir): ");
                firstName = sc.nextLine();
                System.out.println("Cognom a cercar (en blanc per sortir): ");
                lastName = sc.nextLine();
                System.out.println("Data minima a cercar en format yyyy-mm-dd (en blanc per sortir): ");
                dateMin = sc.nextLine();
                System.out.println("Data maxima a cercar en format yyyy-mm-dd (en blanc per sortir): ");
                dateMax = sc.nextLine();
                if ((!firstName.equals(""))||(!lastName.equals(""))||(!dateMin.equals(""))||(!dateMax.equals(""))) {
                    st.setString(1, firstName);
                	st.setString(2, lastName);
                	st.setString(3, dateMin);
                	st.setString(4, dateMax);
                    try (ResultSet rs = st.executeQuery()) {
                        while (rs.next()) {
                            String title = rs.getString("title");
                            int amount = rs.getInt("amount");
                            LocalDate payment_date = rs.getDate("payment_date").toLocalDate();
                            System.out.println(title + "\t" + amount + "\t" + payment_date.format(formatter));
                        }
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println("Error SQL: " + e.getMessage());
        }

        sc.close();
	}

}