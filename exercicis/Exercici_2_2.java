import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.Scanner;

//Modifica l'exercici 3 per demanar a l'usuari el nom i cognom del client que s'ha de mostrar.
public class Exercici_2_2 {

	public static void main(String[] args) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		Scanner sc = new Scanner(System.in);
		String firstName=" ";
		String lastName=" ";
        String url = "jdbc:mysql://localhost:3306/sakila";
        Properties connectionProperties = new Properties();
        connectionProperties.setProperty("user", "root");
        try (Connection con = DriverManager.getConnection(url,connectionProperties);
        		PreparedStatement st = con.prepareStatement(
                        "SELECT r.return_date, s.store_id, f.title FROM rental r "
            		+ "LEFT JOIN (customer c LEFT JOIN store s ON (c.store_id = s.store_id)) "
            		+ "ON (r.customer_id = c.customer_id) "
            		+ "LEFT JOIN (inventory i LEFT JOIN film f ON (i.film_id = f.film_id)) "
            		+ "ON (r.inventory_id = i.inventory_id) "
            		+ "WHERE ((UPPER(c.first_name)= ?)&&(UPPER(c.last_name)= ?)&&(r.return_date IS NOT NULL))")) {
            System.out.println("Base de dades connectada!");
            while (!lastName.equals("")) {
                System.out.println("Nom a cercar (en blanc per sortir): ");
                firstName = sc.nextLine();
                System.out.println("Cognom a cercar (en blanc per sortir): ");
                lastName = sc.nextLine();
                if ((!firstName.equals(""))||(!lastName.equals(""))) {
                    st.setString(1, firstName);
                	st.setString(2, lastName);
                    try (ResultSet rs = st.executeQuery()) {
                        while (rs.next()) {
                            String title = rs.getString("title");
                            int store_id = rs.getInt("store_id");
                            LocalDate return_date = rs.getDate("return_date").toLocalDate();
                            //lastName = rs.getString("last_name");
                            System.out.println(store_id + "\t" + title + "\t" + return_date.format(formatter));
                        }
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println("Error SQL: " + e.getMessage());
        }

        sc.close();
	}

}
