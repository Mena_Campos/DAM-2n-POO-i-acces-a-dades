import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.Scanner;

/*Fes un programa que permeti als usuaris consultar les pel�l�cules disponibles en una botiga. 
Primer, es mostraran totes les botigues, amb la seva adre�a.
Un cop seleccionada una botiga, l'usuari podr� realitzar cerques pel nom de les pel�l�cules, 
pel nom dels actors o pel g�nere de les pel�l�cules.
El resultat d'aquestes cerques mostrar� el t�tol de les pel�l�cules d'aquesta botiga que compleixin els requisits, 
i si estan disponibles, o si no ho estan, la seva data de retorn.
A trav�s de la llista de pel�l�cules, se'n pot seleccionar una, i se'n mostraran els detalls: t�tol, descripci�, 
any de producci�, nacionalitat i actors que hi apareixen.
Separa la interacci� amb l'usuari de la gesti� de la base de dades, de manera que aquest codi sigui reaprofitable.*/
public class Exercici_2_5 {

	public static void main(String[] args) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		Scanner sc = new Scanner(System.in);
		int store = 0;
		String search = " ";
		String film = " ";
        String url = "jdbc:mysql://localhost:3306/sakila";
        Properties connectionProperties = new Properties();
        connectionProperties.setProperty("user", "root");
        
        //Mostrar botigues
        try (Connection con = DriverManager.getConnection(url,connectionProperties);
        		Statement st = con.createStatement();) {
            	System.out.println("Base de dades connectada!");
                    try (ResultSet rs = st.executeQuery("select s.store_id, a.address from store s "
            				+ "left join address a on(s.address_id=a.address_id)")) {
                        while (rs.next()) {
                            int store_id = rs.getInt("store_id");
                            String address = rs.getString("address");
                            System.out.println(store_id + "\t" + address);
                        }
                    }
        } catch (SQLException e) {
            System.err.println("Error SQL: " + e.getMessage());
        }
        
        //Seleccionar botiga y buscar pelicules
        System.out.println("Escull una botiga per la seva Id: ");
        store = sc.nextInt();
        
        try (Connection con2 = DriverManager.getConnection(url,connectionProperties);
        		PreparedStatement st2 = con2.prepareStatement("select f.title, r.return_date from film f "
        				+ "left join inventory i on(f.film_id=i.film_id) "
        				+ "left join rental r on(i.inventory_id=r.inventory_id) "
        				+ "left join film_actor fa on(fa.film_id=f.film_id) "
        				+ "left join actor a on(a.actor_id=fa.actor_id) "
        				+ "left join film_category fc on(f.film_id=fc.film_id) "
        				+ "left join category c on(fc.category_id=c.category_id) "
        				+ "where (((f.title like '?') or (a.first_name like '?') or (c.name like '?')) and (i.store_id like ?))")) {
        	
        		System.out.println("Cercar per nom de pelicules, nom d'actors o genere de pelicules: ");
        		search = sc.nextLine();
        		st2.setString(1, "%" + search + "%");
        		st2.setString(2, "%" + search + "%");
        		st2.setString(3, "%" + search + "%");
        		st2.setInt(4, store);
        	
        		try (ResultSet rs2 = st2.executeQuery()) {
                    while (rs2.next()) {
                        String title = rs2.getString("title");
                        LocalDate return_date = rs2.getDate("return_date").toLocalDate();
                        System.out.println(title + "\t" + return_date.format(formatter));
                    }
                }
                    
        } catch (SQLException e) {
            System.err.println("Error SQL: " + e.getMessage());
        }
        
        //Escollir pelicula i mostrarla
        
        try (Connection con3 = DriverManager.getConnection(url,connectionProperties);
        		PreparedStatement st3 = con3.prepareStatement("select f.title, f.description, f.release_year, l.name, a.first_name, a.last_name from film f "
        				+ "left join language l on(l.language_id=f.language_id) "
        				+ "left join film_actor fa on(f.film_id=fa.film_id) "
        				+ "left join actor a on(fa.actor_id=a.actor_id) "
        				+ "where (f.title like '?')")) {
        	
        	while(!film.equals(" ")){
        		System.out.println("Mostrar pelicula pel seu nom (en blanc per sortir): ");
        		film = sc.nextLine();
        		st3.setString(1, film);
        	
        		try (ResultSet rs3 = st3.executeQuery()) {
                    while (rs3.next()) {
                        String title = rs3.getString("title");
                        String description = rs3.getString("description");
                        int release_year = rs3.getInt("release_year");
                        String name = rs3.getString("name");
                        String first_name = rs3.getString("first_name");
                        String last_name = rs3.getString("last_name");
                        System.out.println(title + "\t" + description + "\t" + release_year + "\t" + name + "\t" + first_name + "\t" + last_name);
                    }
        	}
        }
                    
        } catch (SQLException e) {
            System.err.println("Error SQL: " + e.getMessage());
        }

        sc.close();

	}
}
