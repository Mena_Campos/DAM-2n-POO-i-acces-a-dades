package agenda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Agenda {
	
	private List<Contacte> contactes = new ArrayList<Contacte>();
	
	public void afegeix(Contacte c) throws IllegalArgumentException {
		int pos = Collections.binarySearch(contactes, c);
		
		if (pos >= 0)
			throw new IllegalArgumentException("El contacte ja hi és");
		else
			contactes.add(-pos-1, c);
	}
	
	public boolean elimina(String nom, String cognoms) {
		Contacte c = new Contacte(nom, cognoms, "", "");
		int pos = Collections.binarySearch(contactes, c);
		boolean modificat = false;
		
		if (pos >= 0) {
			contactes.remove(pos);
			modificat = true; 
		}
		return modificat;
	}
	
	public boolean modifica(String nom, String cognom, Contacte nouContacte) {
		boolean modificat = elimina(nom, cognom);
		
		if (modificat) {
			afegeix(nouContacte);
		}
		return modificat;
	}
	
	public Contacte cerca(String nom, String cognoms) {
		Contacte c = new Contacte(nom, cognoms, "", "");
		Contacte cerca = null;
		int pos = Collections.binarySearch(contactes, c);
		
		if (pos >= 0) {
			cerca = contactes.get(pos);
		}
		return cerca;
	}
	
	
	
	public List<Contacte> cerca(Contacte criteri) {
		List<Contacte> resultat = new LinkedList<Contacte>(contactes);
		//ListIterator<Contacte> it;
		//Contacte c;
		
		criteri.cercaIElimina(resultat, Contacte.COMPARA_PER_NOM);
		criteri.cercaIElimina(resultat, Contacte.COMPARA_PER_COGNOMS);
		criteri.cercaIElimina(resultat, Contacte.COMPARA_PER_ADRECA);
		criteri.cercaIElimina(resultat, Contacte.COMPARA_PER_TELEFON);
		/*if (!criteri.getNom().equals("")) {
			it = resultat.listIterator();
			while (it.hasNext()) {
				c = it.next();
				if (!c.getNom().equals(criteri.getNom()))
					it.remove();
			}
		}
		if (!criteri.getCognoms().equals("")) {
			it = resultat.listIterator();
			while (it.hasNext()) {
				c = it.next();
				if (!c.getCognoms().equals(criteri.getCognoms()))
					it.remove();
			}
		}
		if (!criteri.getAdreca().equals("")) {
			it = resultat.listIterator();
			while (it.hasNext()) {
				c = it.next();
				if (!c.getAdreca().equals(criteri.getAdreca()))
					it.remove();
			}
		}
		if (!criteri.getTelefon().equals("")) {
			it = resultat.listIterator();
			while (it.hasNext()) {
				c = it.next();
				if (!c.getTelefon().equals(criteri.getTelefon()))
					it.remove();
			}
		}*/
		return resultat;
	}
}
