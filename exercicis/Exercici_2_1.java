import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

//Fes un programa que demani a l'usuari el cognom d'un actor i que mostri per pantalla totes les seves pel�l�cules.
public class Exercici_2_1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
        String lastName=" ";
        String url = "jdbc:mysql://localhost:3306/sakila";
        Properties connectionProperties = new Properties();
        connectionProperties.setProperty("user", "root");
        try (Connection con = DriverManager.getConnection(url,connectionProperties);
        		PreparedStatement st = con.prepareStatement(
                        "SELECT title "
                        + "FROM film WHERE film_id IN (SELECT film_id "
                        + "FROM film_actor WHERE actor_id IN (SELECT actor_id "
                        + "FROM actor WHERE last_name LIKE ?))")) {
            System.out.println("Base de dades connectada!");
            while (!lastName.equals("")) {
                System.out.println("Cognom a cercar (en blanc per sortir): ");
                lastName = sc.nextLine();
                if (!lastName.equals("")) {
                    st.setString(1, lastName);
                    try (ResultSet rs = st.executeQuery()) {
                        while (rs.next()) {
                            String title = rs.getString("title");
                            //lastName = rs.getString("last_name");
                            System.out.println(title);
                        }
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println("Error SQL: " + e.getMessage());
        }

        sc.close();

	}

}
